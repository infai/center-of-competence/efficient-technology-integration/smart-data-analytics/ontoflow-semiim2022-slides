---
theme: default
class: invert
---
# OntoFlow

a user-friendly Ontology Development Workflow

<!-- footer: SemIIM22 - InfAI - Gordian Dziwis, Lisa Wenige, Lars-Peter Meyer, Michael Martin -->

---

<!-- paginate: true -->
<!-- header: OntoFlow: A user-friendly Ontology Development Workflow -->

Motivation
Requirements
Architecture
Evaluation

<!-- footer: SemIIM22 - InfAI - Gordian Dziwis -->

---

# Motivation

---

<!-- class: default -->

Issues with existing ontologies

- Discoverability
- Quality
- Contribution

---

![h:500](demo.gif)

---

<!-- _class: invert -->

# Requirements

---

OntoFlow should automatically

- validate
- post-process
- serialize
- document
- publicize

the ontology

---

OntoFlow should be user-friendly

- Easy and fast to execute
- Easy to modify
- GUI Support

---

<!-- _class: invert -->
# Architecture

---

- validate
- post-process
- serialize
- document
- publicize

---

- validate ⇾ pySHACL
- post-process ⇾ sparql-integrate
- serialize ⇾ Jena Riot
- document ⇾ pyLODE
- publicize ⇾ GitLab repository/pages

---

- validate ⇾ pySHACL
- post-process ⇾ sparql-integrate
- serialize ⇾ Jena Riot
- document ⇾ pyLODE
- publicize ⇾ GitLab repository/pages
- GUI support ⇾ draw.io with Chowlk

![bg right fit](copper.png)

---

![h:445px](workflow-0.png)

---


![h:445px](workflow-1.png)

---


![h:445px](workflow-2.png)

---


![bg 70%](architecture-0.png)

---


![bg 70%](architecture-1.png)

---


![bg 70%](architecture-2.png)

---

![bg 70%](architecture-3.png)

---

![bg 70%](architecture-4.png)

---

<!-- _class: invert -->
# Evaluation

---

## Fast to execute?

---

<!-- _header: Execution duration of 799 ontologies from Linked Open Vocabularies -->

![bg 100%](benchmark.png)

---

## Easy to execute?

---

### Setup with GitLab

Add CI job file to your repository.

---

### Run locally

With docker and java installed:
```bash
curl -s https://get.nextflow.io | bash
cd PATH-OF-YOUR-ONTOLGY
nextflow pull https://gitlab.com/infai/ontoflow -r main
nextflow run https://gitlab.com/infai/ontoflow -r main
```

---

## Easy to modify?

---

Workflow script is 241 LOC.

---

```groovy
process createDocumentation {

    container params.images.registry + 'pylode:' + params.images.tag

    input:
    path 'index.ttl' from ontology.documentation

    output:
    path 'index.html'

    """
    pylode index.ttl -o index.html
    """

}
```

---

```groovy
process getPriorVersionFromOntology {

    container params.images.registry + 'sparql-integrate:' + params.images.tag

    input:
    path ontology from ontology.priorVersion

    output:
    stdout priorVersionUriJson

    """
    #!/usr/bin/env -S rpt integrate --jq $ontology

    prefix owl:   <http://www.w3.org/2002/07/owl#>

    SELECT ?version WHERE {

      ?ontology a owl:Ontology ;
        owl:priorVersion ?version .
    }
    """

}
```

---

## (Optional) Experiences from the KupferDigital project

- No need for infrastructure
- Hidden feedback from failed jobs
- Chowlk has potential and should be used with care

---

<!-- _class: invert -->
# Questions and Answers

dziwis@infai.org
https://gitlab.com/infai/ontoflow
